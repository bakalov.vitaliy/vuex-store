import store from './store'

import 'normalize.css/normalize.css'
import './assets/stylesheets/index.scss'

import Vue from 'vue'
import i18n from './services/locale'
import App from './App.vue'

new Vue({
  el: '#app',
  store,
  i18n,
  render: h => h(App)
})
